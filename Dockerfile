From ubuntu:22.04

LABEL maintainer="matega <matega@gmx.de>"

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt dist-upgrade -y && apt-get install -y --no-install-recommends \
    apt-utils xz-utils sudo locales software-properties-common wget curl ffmpeg lame
ENV LANGUAGE de_DE.UTF-8
ENV LANG de_DE.UTF-8
ENV LC_ALL de_DE.UTF-8

RUN sed -i 's/^# de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen && dpkg-reconfigure locales && update-locale LANG=${LANG} LC_ALL=${LANG} LANGUAGE=${LANG}

# open-jdk8
RUN apt-get update && apt-get install -y --no-install-recommends \
    openjdk-8-jdk

# Temurin JDK 8 funktioniert nicht
# RUN curl -kfsSL https://github.com/adoptium/temurin8-binaries/releases/download/jdk8u422-b05/OpenJDK8U-jdk_x64_linux_hotspot_8u422b05.tar.gz -o /tmp/java.tar.gz && \
#    mkdir -p /usr/lib/jvm/temurinjdk-8-x64 && \
#    tar xfz /tmp/java.tar.gz --strip-components=1 -C /usr/lib/jvm/temurinjdk-8-x64 && \
#    rm /tmp/java.tar.gz
# Setze JAVA_HOME und PATH
#ENV JAVA_HOME=/usr/lib/jvm/temurinjdk-8-x64
#ENV PATH="$JAVA_HOME/bin:$PATH"
# Überprüfe die Java-Installation
#RUN java -version

# trivy (Security Scan)
ARG TRIVY_VERSION=0.56.1
RUN curl -fsSL https://github.com/aquasecurity/trivy/releases/download/v${TRIVY_VERSION}/trivy_${TRIVY_VERSION}_Linux-64bit.tar.gz -o /tmp/trivy.tar.gz && \
    mkdir -p /usr/lib/trivy && \
    tar xvfz /tmp/trivy.tar.gz -C /usr/lib/trivy && \
    ln -s /usr/lib/trivy/trivy /usr/local/bin/trivy && \
    rm /tmp/trivy.tar.gz

# tini variables
ARG TINI_VERSION=0.19.0
# downlaod tini (beendet Prozesse)
RUN curl -kfsSL https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini -o /bin/tini && \
    chmod +x /bin/tini

EXPOSE 4040
VOLUME /mnt/music
VOLUME /opt/app/state

# Wenn das DEB-Paket nicht mehr herunterladbar ist, dann lokale Datei verwenden
RUN wget "https://s3-eu-west-1.amazonaws.com/subsonic-public/download/subsonic-6.1.6.deb" -O /tmp/subsonic.deb && \
    dpkg -i /tmp/subsonic.deb && \
    rm -rf /tmp/subsonic.deb

RUN wget https://johnvansickle.com/ffmpeg/releases/ffmpeg-release-amd64-static.tar.xz -O /tmp/ffmpeg-release-64bit-static.tar.xz && \
    mkdir -p /opt/ffmpeg && \
    tar xf /tmp/ffmpeg-release-64bit-static.tar.xz -C /opt/ffmpeg --strip-components=1 && \
    apt autoclean && \
    apt autoremove -y && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /opt/app
ENV SUBSONIC_CONTEXT_PATH /
ENV SUBSONIC_MAX_MEMORY 1024

RUN mkdir -p /opt/app

ADD startup.sh /opt/app/
RUN chmod +x /opt/app/startup.sh

ENTRYPOINT /opt/app/startup.sh 1000